#!/usr/bin/env python
import logging
from contextlib import suppress
from io import TextIOWrapper
from pathlib import Path

import typer

logging.basicConfig()
logger = logging.getLogger(__name__)


# indexed name → name
rindex: dict[int, str] = {}
# name → indexed name
rrindex: dict[str, int] = {}


def _dump_entry(f: TextIOWrapper, i: int, name: str):
        f.write(f"{i}\x00{name}\n")

def dump_index(filepath: Path):
    with filepath.open(mode="w") as f:
        for i, name in rindex.items():
            _dump_entry(f, i, name)


def _load_index(filepath: Path):
    with suppress(FileNotFoundError):
        with filepath.open() as f:
            for line in f:
                i, name = line.split("\x00", maxsplit=1)
                yield int(i), name[:-1]  # remove \n


def load_index(filepath: Path):
    for i, name in _load_index(filepath):
        index_path(i, name)


def index_path(i: int, name: str):
    rindex[i] = name
    rrindex[name] = i


def build_rrindex():
    rrindex.update((name, i) for i, name in rindex.items())


def index_io_open(index_path: Path):
    if index_path.exists():
        return index_path.open(mode="a")
    else:
        index_path.parent.mkdir(parents=True, exist_ok=True)
        return index_path.open(mode="w")


def build_index(directory: Path, index_path: Path):
    i_max = max(rindex, default=0)
    with index_io_open(index_path) as f:
        for p in directory.glob("**/*"):
            if p.is_dir():
                continue

            name = str(p.relative_to(directory))
            if name in rrindex:
                continue

            i_max += 1
            yield i_max, name
            _dump_entry(f, i_max, name)


def _link(src_file: Path, dest_file: Path):
    if src_file.exists():
        dest_file.parent.mkdir(parents=True, exist_ok=True)
        with suppress(FileExistsError):
            dest_file.symlink_to(src_file.absolute())
            logger.info(f"link {dest_file} → {src_file}")
    else:
        with suppress(FileNotFoundError):
            dest_file.unlink()
            logger.info(f"unlink {dest_file}")


def link_to_index(src_dir: Path, dest_dir: Path, index_file: Path):
    load_index(index_file)

    checked = set[int]()

    for i, name in build_index(src_dir, index_file):
        dest_file = dest_dir / str(i)
        src_file = src_dir / name

        _link(src_file, dest_file)

        checked.add(i)

    # sanity check
    to_check = rindex.keys() - checked
    for i in to_check:
        name = rindex[i]
        dest_file = dest_dir / str(i)
        src_file = src_dir / name

        _link(src_file, dest_file)


def link_to_files(src_dir: Path, dest_dir: Path, index_file: Path):
    load_index(index_file)

    for i, name in rindex.items():
        src_file = src_dir / str(i)
        dest_file = dest_dir / name

        if src_file.exists():
            dest_file.parent.mkdir(parents=True, exist_ok=True)
            with suppress(FileExistsError):
                dest_file.symlink_to(src_file.absolute())
                logger.info(f"link {dest_file} → {src_file}")


app = typer.Typer()


@app.command()
def index(src_dir: Path, dest_dir: Path):
    index_file = dest_dir / "index"
    link_to_index(src_dir, dest_dir, index_file)


@app.command()
def link(src_dir: Path, dest_dir: Path):
    index_file = src_dir / "index"
    link_to_files(src_dir, dest_dir, index_file)


if __name__ == '__main__':
    app()
